package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import myprojects.automation.assignment2.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CheckMainMenuTest extends BaseScript{

    public static void main(String[] args) throws InterruptedException {
       // TODO Script to check Main Menu items

        WebDriver driver = getDriver();

//        login(driver);
        driver.navigate().to(Properties.getBaseAdminUrl());
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("webinar.test@gmail.com");
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");
        password.submit();

        WebDriverWait waitMenu = new WebDriverWait(driver, 10);
       waitMenu.until (ExpectedConditions.visibilityOfElementLocated(By.className("menu")));

        List <WebElement> menuLiLevelOne = driver.findElements(By.cssSelector(".maintab"));

     for(WebElement el : menuLiLevelOne) {

         el.click();
            driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

            String location = driver.getCurrentUrl();
            driver.navigate().refresh();
            driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
//            waitMenu.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".maintab")));

            if (driver.getCurrentUrl().equals(location)) {
                System.out.println("The link at " + driver.getTitle() + "is the same after refresh");
            }
        }


        driver.close();
    }


}
