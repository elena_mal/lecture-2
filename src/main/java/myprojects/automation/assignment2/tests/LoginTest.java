package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import myprojects.automation.assignment2.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTest extends BaseScript {

//    static WebDriver driver = getDriver();
//    public static void login() {
//
//
//        driver.navigate().to(Properties.getBaseAdminUrl());
//        WebElement email = driver.findElement(By.id("email"));
//        email.sendKeys("webinar.test@gmail.com");
//        WebElement password = driver.findElement(By.id("passwd"));
//        password.sendKeys("Xcg7299bnSmMuRLp9ITw");
//        password.submit();
//        WebDriverWait wait = new WebDriverWait(driver, 10);
//        wait.until (ExpectedConditions.visibilityOfElementLocated(By.id("employee_infos")));
//    }
//        public static WebDriverWait waitForLoad(WebDriver driver) {
//            WebDriverWait wait = new WebDriverWait(driver, 10);
//            return wait;
//        }

    public static void main(String[] args) throws InterruptedException {
        // TODO Script to execute login and logout steps
//        WebDriver driver = getDriver();
//        WebDriver s = getLogin();

        WebDriver driver = getDriver();
        driver.navigate().to(Properties.getBaseAdminUrl());
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("webinar.test@gmail.com");
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");
        password.submit();



        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("employee_infos")));
        System.out.println("Logged in successfully!");


        driver.findElement(By.id("employee_infos")).click();
        driver.findElement(By.id("header_logout")).click();
        System.out.println("Logged out successfully!");


        driver.close();
    }


}
